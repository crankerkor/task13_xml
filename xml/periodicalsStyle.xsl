<?xml version = "1.0" ?>
<xsl:stylesheet
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
        xmlns:x="urn:Papers.Namespace">
    <xsl:template match = "/">
        <html>
            <head>
                <style type = "text/css">
                    table {
                      border: 2px double black;
                      border-color: black;
                      border-collapse: collapse;
                      background-color: white ;
                      color: black ;
                      text-align: center;
                    }
                    th, td {
                      border: 2px double grey;
                    }
                </style>
            </head>

            <body>
                <table>
                    <tr>
                        <th style = "width: 100px">Title</th>
                        <th style = "width: 100px">Type</th>
                        <th>Monthly</th>
                        <th>Pages</th>
                        <th>Glossy</th>
                        <th>Index</th>
                    </tr>

                    <xsl:for-each select="//x:papers//x:paper">
                        <tr>
                            <td>
                                <xsl:value-of select="@title"/>
                            </td>
                            <td>
                                <xsl:value-of select="x:type"/>
                            </td>
                            <td>
                                <xsl:value-of select="x:isMonthly"/>
                            </td>
                            <td>
                                <xsl:value-of select="x:chars/x:pages"/>
                            </td>
                            <td>
                                <xsl:value-of select="x:chars/x:isGlossy"/>
                            </td>
                            <td>
                                <xsl:value-of select="x:chars/x:index"/>
                            </td>

                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>