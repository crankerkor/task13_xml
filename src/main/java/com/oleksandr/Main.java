package com.oleksandr;

import com.oleksandr.xml.dom.DOMParserUser;
import com.oleksandr.xml.html.XMLToHTML;
import com.oleksandr.xml.model.Paper;
import com.oleksandr.xml.sax.SAXParserUser;
import com.oleksandr.xml.stax.StaxReader;

import java.io.File;
import java.util.List;

public class Main {

  public static void main(String[] args) {
    File xml = new File("xml\\periodicals.xml");
    File xsd = new File("xml\\periodicalsScheme.xsd");

    XMLToHTML converter = new XMLToHTML();
    converter.convertXMLToHTML();

      //            SAX
      //printList(SAXParserUser.parsePapers(xml, xsd), "SAX");

      // StAX
      //printList(StaxReader.parsePapers(xml, xsd), "StAX");

      // DOM
      //printList(DOMParserUser.getPaperList(xml, xsd), "DOM");
  }

  private static void printList(List<Paper> papers, String parserName) {
    System.out.println(parserName);
    papers.forEach(System.out::println);
  }
}
