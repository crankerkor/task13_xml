package com.oleksandr.xml.dom;

import com.oleksandr.xml.model.Paper;
import org.w3c.dom.Document;

import java.util.List;

import java.io.File;

public class DOMParserUser {
  public static List<Paper> getPaperList(File xml, File xsd) {
    DOMDocCreator creator = new DOMDocCreator(xml);
    Document doc = creator.getDocument();

    DOMValidator.validate(DOMValidator.createSchema(xsd), doc);


    DOMDocReader reader = new DOMDocReader();

    return reader.readDoc(doc);
  }
}
