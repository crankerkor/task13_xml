package com.oleksandr.xml.dom;

import com.oleksandr.xml.model.Chars;
import com.oleksandr.xml.model.Paper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {
  public List<Paper> readDoc(Document doc) {
    doc.getDocumentElement().normalize();
    List<Paper> papers = new ArrayList<>();

    NodeList nodeList = doc.getElementsByTagName("paper");

    for (int i = 0; i < nodeList.getLength(); i++) {
      Paper paper = new Paper();
      Chars chars;

      Node node = nodeList.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;

        paper.setTitle(element.getAttribute("title"));
        paper.setMonthly(
                Boolean.parseBoolean(element.getElementsByTagName("isMonthly")
                .item(0).getTextContent())
        );
        paper.setType(
                element.getElementsByTagName("type").item(0).getTextContent());

        chars = getChars(element.getElementsByTagName("chars"));

        paper.setChars(chars);

        papers.add(paper);
      }
    }

    return papers;
  }

  private Chars getChars(NodeList nodes) {
    Chars chars = new Chars();

    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);

      if (element.getElementsByTagName("index").item(0) != null) {
        chars.setIndex(
            Integer.parseInt(element.getElementsByTagName("index")
                    .item(0).getTextContent()));
        }
      chars.setGlossy(
              Boolean.parseBoolean(element.getElementsByTagName("isGlossy")
              .item(0).getTextContent())
      );
      chars.setPages(
              Integer.parseInt(element.getElementsByTagName("pages")
              .item(0).getTextContent())
      );
      chars.setColored(
              Boolean.parseBoolean(element.getAttribute("colored"))
      );
    }

    return chars;
  }
}
