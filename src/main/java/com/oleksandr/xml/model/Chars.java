package com.oleksandr.xml.model;

public class Chars {
  private int pages;
  private boolean isGlossy;
  private int index;
  private boolean colored;

  public Chars() {

  }

  public Chars(int pages, boolean isGlossy,
               int index, boolean colored) {
    this.pages = pages;
    this.isGlossy = isGlossy;
    this.index = index;
    this.colored = colored;
  }

  public void setPages(int pages) {
    this.pages = pages;
  }

  public int getPages() {
    return pages;
  }

  public void setGlossy(boolean glossy) {
    isGlossy = glossy;
  }

  public boolean isGlossy() {
    return isGlossy;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public int getIndex() {
    return index;
  }

  public void setColored(boolean colored) {
    this.colored = colored;
  }

  public boolean isColored() {
    return colored;
  }
}
