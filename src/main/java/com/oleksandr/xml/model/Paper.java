package com.oleksandr.xml.model;

public class Paper {
  private String title;
  private String type;
  private boolean isMonthly;
  private Chars chars;

  public Paper() {

  }

  public Paper(String title, String type,
               boolean isMonthly, Chars chars) {
    this.title = title;
    this.type = type;
    this.isMonthly = isMonthly;
    this.chars = chars;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setMonthly(boolean monthly) {
    isMonthly = monthly;
  }

  public boolean isMonthly() {
    return isMonthly;
  }

  public void setChars(Chars chars) {
    this.chars = chars;
  }

  public Chars getChars() {
    return chars;
  }

  @Override
  public String toString() {
    return "Title: " + title + "\nType: " + type + "\nMonthly: " + isMonthly +
            "\nChars: " + "\nPages: " + chars.getPages() +
            "\nGlossy: " + chars.isGlossy() +
            "\nColored: " + chars.isColored() +
            "\nIndex: " + chars.getIndex();
  }
}
