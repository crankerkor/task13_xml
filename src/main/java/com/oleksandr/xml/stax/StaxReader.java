package com.oleksandr.xml.stax;

import com.oleksandr.xml.model.Chars;
import com.oleksandr.xml.model.Paper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StaxReader {
  public static List<Paper> parsePapers(File xml, File xsd) {
    List<Paper> papers = new ArrayList<>();
    Paper paper = null;
    Chars chars = null;

    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    try {
      XMLEventReader xmlEventReader =
              xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent event = xmlEventReader.nextEvent();
        if (event.isStartElement()) {
          StartElement startElement = event.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case "paper":
              paper = new Paper();

              Attribute titleAttr = startElement
                      .getAttributeByName(new QName("title"));
              if (titleAttr != null) {
                paper.setTitle(titleAttr.getValue());
              }
              break;
            case "type":
              event = xmlEventReader.nextEvent();
              assert paper != null;
              paper.setType(event.asCharacters().getData());
              break;
            case "isMonthly" :
              event = xmlEventReader.nextEvent();
              assert paper != null;
              paper.setMonthly(
                      Boolean.parseBoolean(event.asCharacters().getData())
              );
              break;
            case "chars":
              chars = new Chars();


              Attribute coloredAttr = startElement
                        .getAttributeByName(new QName("colored"));
              if (coloredAttr != null) {
                  chars.setColored(
                        Boolean.parseBoolean(coloredAttr.getValue()
                                )
                );
                  }
              break;
            case "pages" :
              event = xmlEventReader.nextEvent();
              assert chars != null;

              chars.setPages(
                      Integer.parseInt(event.asCharacters().getData())
              );
              break;
            case "isGlossy":
              event = xmlEventReader.nextEvent();
              assert chars != null;
              chars.setGlossy(
                      Boolean.parseBoolean(event.asCharacters().getData())
              );
              break;
            case "index":
              event = xmlEventReader.nextEvent();
              assert chars != null;
              chars.setIndex(
                      Integer.parseInt(event.asCharacters().getData())
              );
              break;
          }
        }

        if (event.isEndElement()) {
          EndElement endElement = event.asEndElement();
          if (endElement.getName().getLocalPart().equals("paper")) {
            paper.setChars(chars);
            papers.add(paper);
          }
        }
      }
    } catch (FileNotFoundException | XMLStreamException ex) {
        ex.printStackTrace();
    }
    return papers;

  }
}
