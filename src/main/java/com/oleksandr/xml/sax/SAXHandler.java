package com.oleksandr.xml.sax;

import com.oleksandr.xml.model.Chars;
import com.oleksandr.xml.model.Paper;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
  private List<Paper> papers = new ArrayList<>();
  private Paper paper;
  private Chars chars;

  private boolean pType;
  private boolean pIsMonthly;
  private boolean pPages;
  private boolean pGlossy;
  private boolean pIndex;

  public void startElement(String uri, String localName,String qName, Attributes attributes) {
    if (qName.equalsIgnoreCase("paper")){
      String titleN = attributes.getValue("title");
      paper = new Paper();
      paper.setTitle(titleN);
    }
    else if (qName.equalsIgnoreCase("type")){pType = true;}
    else if (qName.equalsIgnoreCase("isMonthly")){pIsMonthly = true;}
    else if (qName.equalsIgnoreCase("chars")){
      chars = new Chars();
      boolean isColored = Boolean.parseBoolean
              (attributes.getValue("colored"));
      chars.setColored(isColored);
    }
    else if (qName.equalsIgnoreCase("pages")){pPages = true;}
    else if (qName.equalsIgnoreCase("isGlossy")){pGlossy = true;}
    else if (qName.equalsIgnoreCase("index")){pIndex = true;}
  }

  public void endElement(String uri, String localName, String qName) throws SAXException {
    if (qName.equalsIgnoreCase("paper")){
      paper.setChars(chars);
      papers.add(paper);
    }
  }

  public void characters(char ch[], int start, int length) throws SAXException {
    if (pType){
      paper.setType(new String(ch, start, length));
      pType = false;
    }
    else if (pIsMonthly){
      paper.setMonthly(Boolean.parseBoolean(new String(ch, start, length)));
      pIsMonthly = false;
    }
    else if (pPages){
      int pages = Integer.parseInt(new String(ch, start, length));
      chars.setPages(pages);
      pPages = false;
    }
    else if (pGlossy){
      boolean glossy = Boolean.parseBoolean(new String(ch, start, length));
      chars.setGlossy(glossy);
      pGlossy = false;
    }
    else if (pIndex){
      int index = Integer.parseInt(new String(ch, start, length));
      chars.setIndex(index);
      pIndex = false;
    }

  }

  public List<Paper> getPapers() {
    return papers;
  }
}
