package com.oleksandr.xml.sax;

import com.oleksandr.xml.model.Paper;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SAXParserUser {
  private static SAXParserFactory parserFactory = SAXParserFactory.newInstance();

  public static List<Paper> parsePapers(File xml, File xsd) {
    List<Paper> papers = new ArrayList<>();

    try {
      parserFactory.setSchema(SAXValidator.createSchema(xsd));

      SAXParser parser = parserFactory.newSAXParser();
      SAXHandler handler = new SAXHandler();
      parser.parse(xml, handler);

      papers = handler.getPapers();


    }catch (SAXException | ParserConfigurationException | IOException ex){
      ex.printStackTrace();
    }

    return papers;
  }
}
