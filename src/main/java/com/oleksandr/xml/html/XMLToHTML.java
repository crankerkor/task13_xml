package com.oleksandr.xml.html;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XMLToHTML {

  public void convertXMLToHTML() {
    Source xml = new StreamSource(
            new File("C:\\Users\\sashk\\Desktop\\string\\" +
                    "task13_XML\\xml\\periodicals.xml"));
    Source xslt = new StreamSource(
            "C:\\Users\\sashk\\Desktop\\" +
                    "string\\task13_XML\\xml\\periodicalsStyle.xsl");

    StringWriter sw = new StringWriter();

    try {
      FileWriter fw = new FileWriter(
              "C:\\Users\\sashk\\Desktop\\" +
                      "string\\task13_XML\\xml\\result.html");

      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer(xslt);
      transformer.transform(xml,new StreamResult(sw));

      fw.write(sw.toString());
      fw.close();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
